#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "firgelli.h"


Firgelli *lac;

const int POS_MIN = 200;
const int POS_MAX = 500;
const int SPEED_MIN = 300;
const int SPEED_MAX = 900;



void milliSleep(int ms)
{
	usleep(ms * 1000);
}



void delayWrite(int location, int value)
{
	fprintf(stdout, "loc: %i value: %i \n", location, value);
	lac->WriteCode(location, value);
	milliSleep(30);
}



void setup()
{
	// delayWrite(0x30, 1);  // delay manual

	delayWrite(0x01,10);      // accuracy
	delayWrite(0x02,20);      // retract limit
	delayWrite(0x03,1000);    // extend limit
	delayWrite(0x04,3);       // movement threshold
	delayWrite(0x05,10000);   // stall time
	delayWrite(0x06,80);      // PWM threshold
	delayWrite(0x07,3);       // Derivative Threshold
	delayWrite(0x08,1023);    // max derivative
	delayWrite(0x09,0);       // min derivative
	delayWrite(0x0a,1023);    // max pwm
	delayWrite(0x0b,0);       // min pwm
	delayWrite(0x0c,1);       // Kp
	delayWrite(0x0d,10);      // Kd
	delayWrite(0x0e,4);       // average RC
	delayWrite(0x0f,8);       // average ADC
	delayWrite(0x21,500);    // speed
}




int moveTo(int target)
{
	lac->WriteCode(0x20, target);
	int lastPos = -2;
	int pos = -1;
	int numberOfSameReadings = 0;
	while (numberOfSameReadings < 10) {
		lastPos = pos;
		pos = lac->WriteCode(0x10, 0);
		//fprintf(stdout, "pos %i\n", pos);
		if (lastPos == pos) numberOfSameReadings++;
		milliSleep(50);
	}
	fprintf(stdout, "pos: %i\n", pos);
	return pos;
}


int getPos()
{
	return lac->WriteCode(0x10, 0);
}



void setSpeed(int speed)
{
	lac->WriteCode(0x21, speed);
}



int getRandom(int lower, int upper)
{
	return lower + (rand() % (upper - lower));
}

void speedTargetPause(int speed, int target, int ms)
{
	// debug
	// fprintf(stdout, "speed: %i  target: %i  pause: %i  \n", speed, target, ms);
	// milliSleep(ms);
	// return;

	// real
	setSpeed(speed);
	int pos = moveTo(target);
	int err = target - pos;
	fprintf(stdout, "speed: %i  target: %i  pos: %i  err: %i \n", speed, target, pos, err);
	milliSleep(ms);
}


//------------------------------------------------------------------------------

void rando0(int count)
{
	for(int i=0; i<count; i++) {

		// move in
		int speed = getRandom(SPEED_MIN, SPEED_MAX);
		int target = getRandom(POS_MIN, POS_MIN + 100);
		int pause = getRandom(500, 1500);
		speedTargetPause(speed, target, pause);


		// back out
		speed = getRandom(200, 600);
		target = getRandom(200, 300);
		pause = getRandom(1000, 2000);
		speedTargetPause(speed, target, pause);
	}
}


//------------------------------------------------------------------------------

void rando1(int count)
{
	for(int i=0; i<count; i++) {
		int speed = getRandom(SPEED_MIN, SPEED_MAX);
		int target = getRandom(POS_MIN, POS_MAX);
		int pause = getRandom(500, 2000);
		speedTargetPause(speed, target, pause);
	}
}

//------------------------------------------------------------------------------

int main(int argc, char *argv[])
{

	/*

	*/

	lac = new Firgelli();
	lac->Open(1);
	// lac->SetDebug(true);
	lac->Info();
	milliSleep(100);
	srand(time(NULL));
	setup();


	while(true) {
		int count = getRandom(5, 10);
		int flip = getRandom(0, 3);
		printf("flip: %i, count: %i \n", flip, count);
		if (flip == 0) {
			rando0(count);
		} else if (flip == 1) {
			rando1(count);
		} else {
			milliSleep(getRandom(1000, 5000));
		}
	}

}
