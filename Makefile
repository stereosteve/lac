CPPFLAGS=-g  -Wall
CC=g++

USB=-lusb-1.0
LAC=lac.o firgelli.o


SCRIPTS=setall
EXECS=lac minion
all: $(EXECS)

firgelli.o: firgelli.cpp firgelli.h
lac.o: lac.cpp firgelli.h
minion.o: minion.cpp

lac:	$(LAC)
	$(CC) $(LAC)  -o lac -lm $(USB)

minion: minion.o firgelli.o
	$(CC) firgelli.o minion.o -o minion -lm $(USB)

clean:
	rm *.o $(EXECS)
